# README #

Platformski nezavisno programiranje - Djinni tool

### Sadržaj ###

* DjinniCPP.docx - Kratak seminar o Djinni alatu, i o projektu. (Također i u PDF formatu)
* PNPFaktorialDjinni - rar datoteka koja sadrži: 
* * pnpfakt.djinni IDL datoteku
* * run_djinni.sh skriptu za porketanje djinnia
* * 'generated-src' folder koji sadrži sve generirane datoteke koje je Djinni kreirao
* * 'src' folder koji sadrži implementaciju u C++ jeziku (core funkcionalnost programa).
* android_project - rar datoteka koja sadrži cjelokupan Android Studio Projekt (mislim da sam tu zeznuo jer sam mogao samo 'src' folder stavit al za svaki slučaj neka bude tu)
* final_apk - rar datoteka koja sadrži .apk datoteku za instalaciju aplikacije na Android mobilne uređaje. (minimum android verzija - API 15 (4.0.3 - Ice Cream Sandwich))

### Autor ###

* Ajanović Mersad